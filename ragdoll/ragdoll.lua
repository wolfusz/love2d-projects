-- COLLISIONS GROUPS {
    GROUP_COLLIDE = 1
--}

-- COLLISIONS CATEGORIES {
    PLAYER = 1
    ENVIRONMENT = 2
    RAGDOLL_HEAD = 10
    RAGDOLL_TORSO = 11
    RAGDOLL_LIMB = 12
--}

local LAST_RAGDOLL = 0

-- RAGDOLL CLASS
RAGDOLL = {parts = {},joints = {}}

function RAGDOLL:newPart(world, x, y, cd)
    -- create body in world
    local body = love.physics.newBody(world, x+cd.x, y+cd.y, "dynamic")
    body:setAngle(math.rad(cd.a))
    --LOG(cd.mass)
    body:setMassData(0, 0, cd.mass[1], cd.mass[2])
    local shape = love.physics.newRectangleShape(0, 0, cd.w, cd.h, 0)
    local part = love.physics.newFixture(body, shape)
    if cd.ct then part:setCategory(unpack(cd.ct)) end
    if cd.m then part:setMask(unpack(cd.m)) end
    part:setFriction(1.0)
    part:setRestitution(0.5)
    --part:setGroupIndex(GROUP_COLLIDE)
    --LOG(cd.desc)
    
    -- additional data
    local data = {}
    data.color = cd.c or {255,255,255}
    data.description = cd.desc or ""
    part:setUserData(data)
    
    table.insert(self.parts, part)
    return part
end

function RAGDOLL:jointParts(b, p, l, col)
    b1 = self.parts[b[1]]:getBody()
    b2 = self.parts[b[2]]:getBody()
    -- translate position
    local x, y = b1:getWorldPoint(p[1],p[2])
    -- create joint
    local joint = love.physics.newRevoluteJoint(b1, b2, x, y, col or false)
    local data = {}
    if l ~= nil then
        data.defaults = l
        joint:setLimitsEnabled(true)
        joint:setLimits(math.rad(l[1]),math.rad(l[2]))
    end
    joint:setUserData(data)
    joint:setMaxMotorTorque(0)
    joint:setMotorSpeed(0)
    --joint:setMotorEnabled(false)
    
    table.insert(self.joints, joint)
    return self.joints[#self.joints]
end

function RAGDOLL.new(w, x, y, cd, ad)
    local R = DeepCopy(RAGDOLL)
    LAST_RAGDOLL = LAST_RAGDOLL + 1
    
    -- init default values
    R.conscious = true
    
    -- create body
    for _,data in pairs(cd.parts) do  R:newPart(w, x, y, data)  end
    -- joint parts
    for _,data in pairs(cd.joints) do  R:jointParts( data[1], data[2], data[3] )  end
    
    -- additional data
    for i,v in pairs(ad) do R[i] = v end
    
    return R
end

function RAGDOLL:clone() return DeepCopy(self) end

function RAGDOLL:draw()
    for _,part in pairs(self.parts) do
        love.graphics.setColor(unpack(part:getUserData().color))
        love.graphics.polygon("fill", part:getBody():getWorldPoints( part:getShape():getPoints() ))
        
        if self.DEBUG == true then
            love.graphics.setColor(255,0,0)
            points = pack(part:getBody():getWorldPoints(part:getShape():getPoints()))
            for i=1,#points,2 do
                love.graphics.point(points[i], points[i+1])
            end
        end
    end
    if self.DEBUG == true then
        love.graphics.setColor(255,0,255)
        for _,joint in pairs(self.joints) do
            x1, y1, x2, y2 = joint:getAnchors()
            love.graphics.point(x1,y1)
            love.graphics.point(x2,y2)
        end
    end
end

function RAGDOLL:update(dt)
    for _,part in pairs(self.parts) do
        local s = part:getBody():getAngularDamping()
        if s ~= 0 then LOG(s) end
    end
end