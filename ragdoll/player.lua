PLAYER = {}

function PLAYER.new(w,x,y,c)
    local P = DeepCopy(PLAYER)
    
    P.ragdoll = RAGDOLL.new(w, x, y, {
        parts = { -- part {description, x, y, width, height, angle, color, category, mask},
            -- LEFT ARM
            {   desc = "arm",
                x = 0, y = -1, w = 10, h = 15, a = 0, c = {255, 255, 0}, mass = {2,0},
                ct = { RAGDOLL_LIMB }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            {   desc = "forearm",
                x = 0, y = 12, w = 10, h = 15, a = 0, c = {255, 0, 255}, mass = {2,0},
                ct = { RAGDOLL_LIMB }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            -- LEFT LEG
            {   desc = "thigh",
                x = 0, y = 28, w = 10, h = 20, a = 0, c = {255, 255, 0}, mass = {4,0},
                ct = { RAGDOLL_LIMB }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            {   desc = "calf",
                x = 0, y = 46, w = 10, h = 20, a = 0, c = {255, 0, 255}, mass = {4,0},
                ct = { RAGDOLL_LIMB }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            -- BODY
            {   desc = "neck",
                x = 0, y = -14, w = 7, h = 10, a = 0, c = {100, 100, 100}, mass = {1,0},
                ct = { RAGDOLL_HEAD }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            {   desc = "waist",
                x = 0, y = 14, w = 10, h = 8, a = 0, c = {100, 100, 100}, mass = {3,0},
                ct = { RAGDOLL_TORSO }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            {   desc = "pelvis",
                x = 0, y = 20, w = 17, h = 10, a = 0, mass = {3,0},
                ct = { RAGDOLL_TORSO }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            {   desc = "chest",
                x = 0, y = 0, w = 20, h = 25, a = 0, mass = {4,0},
                ct = { RAGDOLL_TORSO }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            {   desc = "head",
                x = 0, y = -24, w = 15, h = 15, a = 0, mass = {2,0},
                ct = { RAGDOLL_HEAD }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            -- RIGHT LEG
            {   desc = "thigh",
                x = 0, y = 28, w = 10, h = 20, a = 0, c = {255, 255, 0}, mass = {4,0},
                ct = { RAGDOLL_LIMB }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            {   desc = "calf",
                x = 0, y = 46, w = 10, h = 20, a = 0, c = {255, 0, 255}, mass = {4,0},
                ct = { RAGDOLL_LIMB }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            -- RIGHT ARM
            {   desc = "arm",
                x = 0, y = -1, w = 10, h = 15, a = 0, c = {255, 255, 0}, mass = {2,0},
                ct = { RAGDOLL_LIMB }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
            {   desc = "forearm",
                x = 0, y = 12, w = 10, h = 15, a = 0, c = {255, 0, 255}, mass = {2,0},
                ct = { RAGDOLL_LIMB }, m = { RAGDOLL_HEAD, RAGDOLL_TORSO, RAGDOLL_LIMB }
            },
        },
        joints = {
        -- { index{A, B}, position{x, y}, limit{lower, upper} }
        -- TORSO
            -- join waist and chest
            { {6, 8}, {0,-3}, {-20,20} },
            -- join waist and pelvis
            { {6, 7}, {0, 3}, {-20,20} },
            
        -- HEAD
            -- join neck and head
            { {5, 9}, {0,-4}, {-45,90} },
            -- join neck and chest
            { {5, 8}, {0, 4}, {-20,20} },
            
        -- LEFT ARM
            -- join arm and chest
            { {1, 8}, {0,-7}, {-60,210} },
            -- join arm and forearm
            { {1, 2}, {0, 7}, {-180,0} },
            
        -- LEFT LEG
            -- join thigh and pelvis
            { {3, 7}, {0,-8}, {-45,160} },
            -- join thigh and calf
            { {3, 4}, {0, 9}, {-10,175} },
            
        -- RIGHT LEG
            -- join thigh and pelvis
            { {10,  7}, {0,-8}, {-45,160} },
            -- join thigh and calf
            { {10, 11}, {0, 9}, {-10,175} },
            
        -- RIGHT ARM
            -- join arm and chest
            { {12,  8}, {0,-7}, {-60,210} },
            -- join arm and forearm
            { {12, 13}, {0, 7}, {-180,0} },
        }
    },{
        DEBUG = true
    })
    
    P.controls = c or {}
    
    return P
end

function PLAYER:clone() return DeepCopy(self) end

function PLAYER:draw() self.ragdoll:draw() end
function PLAYER:update(dt) self.ragdoll:update(dt) end

function PLAYER:part(p)
--LOG(type(self.ragdoll))
    for _,part in pairs(self['ragdoll'].parts) do
        if part:getUserData().description == p then return part:getBody() end
    end
end

function PLAYER:doControls(k)
    if type(self.controls[k]) == "function" then
        self.controls[k](self)
        return true
    elseif type(self.controls[k]) == "string" then
        f = self.controls[k]
        LOG(f)
        self[f](self)
        return true
    elseif type(self.controls[k]) == "table" then
        f = self.controls[k][1]
        if type(self[f]) == "function" then
            if self.controls[k][2] ~= nil then
                if type(self.controls[k][2]) == "table" then
                    self[f](self,unpack(self.controls[k][2]))
                else
                    self[f](self,self.controls[k][2])
                end
                return true
            end
        end
    end
    return false
end

function PLAYER:changeDir(dir)
    LOG(dir)
    if dir == nil then dir = not self.direction end
    LOG(dir)
    for i,joint in pairs(self.ragdoll.joints) do
        LOG(i)
        LOG({joint:getLimits()})
        local low = math.abs(joint:getLowerLimit())
        local up = math.abs(joint:getUpperLimit())
        if dir == true then
            joint:setLimits( low*(-1), up )
        else
            joint:setLimits( up*(-1), low )
        end
        LOG({joint:getLimits()})
    end
    self.direction = dir
end