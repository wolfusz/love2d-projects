function love.conf(t)
    t.window.title = "Ragdoll"

    t.modules.image = true
    t.modules.joystick = false
    t.modules.physics = true
end


-- for making classes and copying tables
function DeepCopy(obj, seen)
    if type(obj) ~= 'table' then return obj end
    if seen and seen[obj] then return seen[obj] end
    local s = seen or {}
    local res = setmetatable({}, getmetatable(obj))
    s[obj] = res
    for k, v in pairs(obj) do res[DeepCopy(k, s)] = DeepCopy(v, s) end
    return res
end

table.indexOf = function( t, object )
    if "table" == type(t) then
        for i = 1,#t do
            if object == t[i] then
                return i
            end
        end
        return -1
    else
        error("table.indexOf expects table for first argument, " .. type(t) .. " given")
    end
end

function pack(...) return {...} end

-- DEBUGGING
DEBUG = true
function LOG(...)
    if DEBUG ~= true then return end
    local toPrint = {...}
    for k,v in pairs(toPrint) do
        if type(v) == "table" then
            if type(v[1]) == "string" then
                print(tostring(v[1]) .. " = " .. tostring(v[2]))
            else
                t = "" .. v[1]
                for i=2,#v do
                    t = t ..", ".. v[i]
                end
                print("{".. t .."}")
            end
        else
            print(tostring(v))
        end
    end
end

--math.randomseed( os.time() )