CAMERA = {}

function CAMERA:init(w)
    self.body = love.physics.newBody(w,0,0,"static")
end

function CAMERA:getWorldPoint(x,y)
    return self.body:getWorldPoint(x,y)
end

return CAMERA