require("ragdoll")
require("player")
camera = require("camera")

function love.load()
    if DEBUG then love._openConsole() end
    love.graphics.setBackgroundColor(0,0,0)
    
    love.physics.setMeter(32)
    world = love.physics.newWorld(0, 9.81*32, true)
    
    platform = {}
    platform.body = love.physics.newBody(world, 0,0, "static")
    platform.shape = love.physics.newEdgeShape(10,590, 790,590)
    platform.fixture = love.physics.newFixture(platform.body, platform.shape)
    platform.fixture:setCategory(ENVIRONMENT)
    platform.fixture:setGroupIndex(GROUP_COLLIDE)
    
    player = PLAYER.new(world, 300, 500, {
        a = 'changeDir'
    })
    
    camera:init(world)
end

function love.draw()
    -- draw platform
    love.graphics.setColor(255, 255, 255)
    love.graphics.line(platform.body:getWorldPoints(platform.shape:getPoints()))
    
    -- draw player
    player:draw()
    
    if MOUSEJOINT then
        love.graphics.setColor(255, 255, 255)
        love.graphics.line(MOUSEJOINT:getAnchors())
    end
    
    if SELECTEDBODY then
        local r = SELECTEDBODY:getAngle()-math.pi/2
        for i,joint in pairs(SELECTEDBODY:getJointList()) do
            if joint:getType() ~= "mouse" then
                local l1,l2 = joint:getLimits()
                local a = joint:getJointAngle()
                local x, y = joint:getAnchors()
                
                love.graphics.setColor(0,85*i,0)
                love.graphics.circle("line",x,y,5,100)
                love.graphics.arc("fill",x,y,5,l1+r+math.pi,l2+r+math.pi,100)
                
                love.graphics.setColor(255,0,0)
                px = x + 5 * math.cos(a+r+math.pi)
                py = y + 5 * math.sin(a+r+math.pi)
                love.graphics.line(x,y,px,py)
            end
        end
    end
end

function love.update(dt)
    if MOUSEJOINT then
        MOUSEJOINT:setTarget(love.mouse.getPosition())
    end
    
    player:update(dt)
    world:update(dt)
end

function love.keypressed(b)
    if player:doControls(b) then return end
    
    if b=='escape' then love.event.quit() end
end

function love.mousepressed(x,y,b)
    if b=='l' then
        SELECTEDBODY = nil
        local x, y = camera:getWorldPoint(x,y)
        for _,body in pairs(world:getBodyList()) do
            if body ~= camera.body then
                local shape = body:getFixtureList()[1]:getShape()
                if shape:testPoint( body:getX(),body:getY(),body:getAngle(), x,y ) then
                    SELECTEDBODY = body
                    MOUSEJOINT = love.physics.newMouseJoint(body,x,y)
                    break
                end
            end
        end
    end
end

function love.mousereleased(x,y,b)
    if MOUSEJOINT then
        MOUSEJOINT:destroy()
        MOUSEJOINT = nil
    end
end