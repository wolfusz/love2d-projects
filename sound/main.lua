function love.load()
    love.graphics.setColor(0,0,0)
    love.graphics.setBackgroundColor(255,255,255)
    
    len = 2
    rate = 44100
    bits = 16
    channel = 1

    soundData = love.sound.newSoundData(len*rate,rate,bits,channel)

    osc = Oscillator(440)
    amplitude = 0.2

    for i=1,len*rate-1 do
        sample = osc() * amplitude
        soundData:setSample(i, sample)
    end
    
    source = love.audio.newSource(soundData)
    love.audio.play(source)
end

function love.update(dt)
    
end

function Oscillator(freq)
    local phase = 0
    return function()
        phase = phase + 2*math.pi/rate            
        if phase <= 2*math.pi then
            phase = phase - 2*math.pi
        end 
        return math.sin(freq*phase)
    end
end