--( GUI )
GUI = { elements = {}, ELEM = { event = {} } }

--( GUI ELEMENT )
--[[
local ELEM = { origin = { x = { a = 0 }, y = 0 } }
function ELEM:get(field) local v = self for w in string.gmatch(field, "[%w_]+") do v = v[w] end return v end
function ELEM:set(field, value) local v = self for w,d in string.gmatch(field, "([%w_]+)(.?)") do if d == '.' then v[w] = v[w] or {} v = v[w] else v[w] = value end end end

ELEM:set('origin.x.a', 1)
print(ELEM:get('origin.x.a'))
]]
function GUI.ELEM:get(f) -- eg. get('o.x.x.a') returns ELEM.o.x.x.a
    local v = self
    for w in string.gmatch(f, "[%w_]+") do v = v[w] end
    return v
end
function GUI.ELEM:set(f, v) -- eg. set('o.x.x.a', 0) <=> ELEM.o.x.x.a = 0
    local x = self
    for w,d in string.gmatch(f, "([%w_]+)(.?)") do
        if d == '.' then
            x[w] = x[w] or {}
            x = x[w]
        else
            x[w] = v
        end
    end
end

function GUI.ELEM:pointInside(x, y) return x >= self.x and x <= self.x+self.w and y >= self.y and y <= self.y+self.h end

-- EVENTS
function GUI.ELEM:eventM(b) if self.mouseClick then self.mouseClick(b) end end
function GUI.ELEM:eventK(k) if self.keyPress then self.keyPress(k) end end
function GUI.ELEM:eventU(dt) if self.update then self.update(dt) end end

function GUI.ELEM:draw()
    if self.type == "label" then
        love.graphics.setColor(self.color)
        love.graphics.print(self.text, self.x, self.y, self.r, self.scale.x, self.scale.y, self.origin.text.x, self.origin.text.y)
        
    elseif self.type == "button" then
        -- button
        love.graphics.setColor(self.background)
        love.graphics.rectangle('fill', self.x, self.y, self.w, self.h)
        love.graphics.setColor(self.foreground)
        love.graphics.rectangle('line', self.x, self.y, self.w, self.h)
        -- text
        love.graphics.setColor(self.color)
        love.graphics.print(self.text, self.x + self.w/2, self.y + self.h/2, self.r, self.scale.x, self.scale.y, self.origin.text.x, self.origin.text.y)
        
    elseif self.type == "checkbox" then
    
    elseif self.type == "dropdown" then
    
    end
end


function GUI:add(p)
--[[ (in) - i numbers, (s) - string, (t) - table, (f)(...) - function(params)
    p = {
        type(s), x(n), y(n), w(n), h(n), r(n),
        scale{ x(n), y(n) }, origin{ x(n), y(n), text{ x(n), y(n) } },
        background(t(4n)), foreground(t(4n)), color(t(n4)),
        mouseClick(f)(btn), keyPress(f)(key), update(f)(delta)
    }
--]]
    local e = deepcopy(GUI.ELEM)
    --[# set default values
    e.type = p.type or 'label'      -- type: label, button, checkbox, dropdown
    e.text = p.text or ''
    e.x = p.x or 0
    e.y = p.y or 0
    e.w = p.w or 0
    e.h = p.h or 0
    e.r = p.r or 0
    p.scale = p.scale or {}
    e.scale = {
        x = p.scale.x or 1,
        y = p.scale.y or 1
    }
    p.origin = p.origin or { text = {} }
    e.origin = {
        x or 0,
        y or 0,
        text = {
            x = p.origin.text.x or (e.w / 2),
            y = p.origin.text.y or (e.h / 2)
        }
    }
    e.background = p.background or {125,125,125,255}
    e.foreground = p.foreground or {255,255,255,255}
    e.color = p.color or {0,0,0,255}
    for k,v in pairs(p) do print(k,v) if e[k] == nil then e[k] = v end end -- copy attributes from 'p' table
    --#]
    table.insert(self.elements,e)
end

function GUI:draw()
    for _,e in pairs(self.elements) do
        e:draw()
    end
end

function GUI:update(dt)
    for _,e in pairs(self.elements) do
        e:eventU(dt)
    end
end

function GUI:getElemAt(x, y)
    for i = #self.elements,1,-1 do
        local el = self.elements[i]
        if el:pointInside(x,y) then return el end
    end
    return nil
end

function GUI:mouseClick(x, y, b)
    local elem = self:getElemAt(x,y)
    if elem then
        self.activeElem = elem
        self.activeElem:eventM(b)
    end
end

function GUI:keyPress(k)
    local elem = self.activeElem
    if elem then elem:eventK(k) end
end