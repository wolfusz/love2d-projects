function love.load()
    if DEBUG then love._openConsole() end
    love.graphics.setBackgroundColor(0,0,0)
    
    sphere = {
        M = 30, N = 10,
        RX = 0, RY = 0,
        color = {255,255,255},
    }
end

function drawSphere(X,Y,S)
    love.graphics.setColor(unpack(S.color))
    for m = 0,S.M do
        for n = 0,S.N-1 do
            local x = X + 100 * math.sin(math.pi * m/S.M) * math.cos(2 * math.pi * n/S.N)
            local y = Y + 100 * math.sin(math.pi * m/S.M) * math.sin(2 * math.pi * n/S.N)
            local z = X + 100 * math.cos(math.pi * m/S.M)
            
            love.graphics.point(x,y)
            love.graphics.point(x,z)
            love.graphics.point(y,z)
        end
    end
end

function love.draw()
    drawSphere(200,200,sphere)
    love.graphics.print(sphere.RX .. ":" .. sphere.RY, 0, 0)
end

function love.update(dt)
end

function love.keypressed(b)
    if b=='escape' then
        love.event.quit()
    end
    if b==' ' then
        sphere.RX = 0
        sphere.RY = 0
    end
end

function love.mousemoved(x,y,dx,dy)
    if love.mouse.isDown('l') then
        sphere.RX = sphere.RX + dx
        sphere.RY = sphere.RY + dy
    end
end