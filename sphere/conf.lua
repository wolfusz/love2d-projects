function love.conf(t)
    t.window.title = "TEST"
end

-- DEBUGGING
DEBUG = true
function LOG(...)
    if DEBUG ~= true then return end
    local toPrint = {...}
    for k,v in pairs(toPrint) do
        if type(v) == "table" then
            print(tostring(v[1]) .. " = " .. tostring(v[2]))
        else
            print(tostring(v))
        end
    end
end